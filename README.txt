// $Id$

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Credits
 * Contact


INTRODUCTION
------------

Current Maintainer: Mike McDaid <http://drupal.org/user/2722905>

This module takes a Teamtrehouse https://teamtreehouse.com/ username and uses it to pull in their JSON data to populate a pie chart in a block which can be
placed on a page in your Drupal site, great for Portfolio's and CV's. The form for entering the username can be accessed from the site configuration page at /admin/config

I have used the code from http://codepen.io/ethanneff/pen/NPxzxZ and basically extended it into a Drupal module. Read more at https://teamtreehouse.com/community/treehouse-badges-website-widget.


INSTALLATION
------------

1. Install as usual, see http://drupal.org/node/70151 for further information.


CREDITS
------------------------------------------------------------------------------

http://codepen.io/ethanneff/pen/NPxzxZ

CONTACT
------------------------------------------------------------------------------

http://mikemcdaid.co.uk/